
function fileSelected() {
	var file = document.getElementById('file').files[0];
	if (file) {
	  var fileSize = 0;
	  if (file.size > 1024 * 1024)
		fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
	  else
		fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
	
	  document.getElementById('fileDetails').innerHTML = 'Name: ' + file.name + ' (Size: ' + fileSize + ')';
	}
}

function editFileSelected() {
	var file = document.getElementById('fileResponse').files[0];
	if (file) {
	  var fileSize = 0;
	  if (file.size > 1024 * 1024)
		fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
	  else
		fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
	
	  document.getElementById('editFileDetails').innerHTML = 'Name: ' + file.name + ' (Size: ' + fileSize + ')';
	}
}



function uploadProgress(evt) {
if (evt.lengthComputable) {
  var percentComplete = Math.round(evt.loaded * 100 / evt.total);
  document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
}
else {
  document.getElementById('progressNumber').innerHTML = 'unable to compute';
}
}

function uploadComplete(evt) {
/* This event is raised when the server send back a response */
alert(evt.target.responseText);
}

function uploadFailed(evt) {
alert("There was an error attempting to upload the file.");
}

function uploadCanceled(evt) {
alert("The upload has been canceled by the user or the browser dropped the connection.");
}